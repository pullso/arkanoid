const game = {
  width: 640,
  height: 360,
  ctx: undefined,
  platform: undefined,
  ball: undefined,
  rows: 4,
  cols: 8,
  sprites: {
    background: undefined,
    platform: undefined,
    ball: undefined,
    block: undefined
  },
  blocks: [],
  running: true,
  score: 0,
  init() {
    const canvas = document.querySelector('#mycanvas')
    this.ctx = canvas.getContext('2d')
    window.addEventListener('keydown', e => {
      if (e.keyCode === 37 && game.platform.dx > -15) {
        game.platform.dx -= game.platform.velocity
      } else if (e.keyCode === 39 && game.platform.dx < 15) {
        game.platform.dx += game.platform.velocity
      } else if (e.keyCode === 32) {
        game.platform.releaseBall()
      }
    })
    window.addEventListener('keyup', e => {
      this.platform.stop()
    })

  },
  load() {
    for (const s in this.sprites) {
      this.sprites[s] = new Image();
      this.sprites[s].src = "images/" + s + ".png"
    }
  },
  create() {
    for (let row = 0; row < this.rows; row++) {

      for (let col = 0; col < this.cols; col++) {
        this.blocks.push({
          x: 68 * col + 50,
          y: 38 * row + 35,
          width: 64,
          height: 32,
          isAlive: true
        })
      }
    }
  },
  start: function () {
    this.init()
    this.load()
    this.create()
    this.run()

    console.log('game start')
  },
  render() {
    this.ctx.clearRect(0, 0, this.width, this.height)
    this.ctx.drawImage(this.sprites.background, 0, 0)

    this.blocks.forEach(function (b) {
      if (b.isAlive === true) {
        this.ctx.drawImage(this.sprites.block, b.x, b.y)
      }
    }, this)

    this.ctx.drawImage(this.sprites.ball,
      this.ball.width * this.ball.frame, 0,
      this.ball.width, this.ball.height,
      this.ball.x, this.ball.y,
      this.ball.width, this.ball.height)

    this.ctx.drawImage(this.sprites.platform, this.platform.x, this.platform.y)
  },
  update() {
    if (this.ball.collide(this.platform)) {
      this.ball.bumpPlatform(this.platform)
    }
    if (this.platform.dx) {
      this.platform.move()
    }
    if (this.ball.dx || this.ball.dy) {
      this.ball.move()
    }
    this.blocks.forEach(function (b) {
      if (b.isAlive) {
        if (this.ball.collide(b)) {
          this.ball.bumpBlock(b)
        }
      }
    }, this)
    this.ball.checkBounce()
  },
  run() {
    this.update()
    this.render()
    if (!this.running) return
    window.requestAnimationFrame(function () {
      game.run()
    })
  },
  over(msg) {
    this.running = false
    alert(msg)
    window.location.reload()
  }
}

game.ball = {
  width: 22,
  height: 22,
  frame: 0,
  x: 340,
  y: 278,
  dx: 0,
  dy: 0,
  velocity: 3,
  jump() {
    this.dy -= this.velocity
    this.dx -= this.velocity
    this.animate()
  },
  animate() {
    setInterval(function () {
      ++game.ball.frame
      if (game.ball.frame > 3) game.ball.frame = 0
    }, 100)
  },
  move() {
    this.x += this.dx
    this.y += this.dy
  },
  collide(el) {
    let x = this.x + this.dx,
      y = this.y + this.dy

    if (x + this.width > el.x &&
      x < el.x + el.width &&
      y + this.height > el.y &&
      y < el.y + el.height) {
      return true
    }
  },
  bumpBlock(block) {
    this.dy *= -1
    block.isAlive = false
    game.score++
    if (game.score >= game.blocks.length) {
      game.over('You win!')
    }
  },
  checkBounce() {
    let x = this.x + this.dx,
      y = this.y + this.dy
    if (x < 0) {
      this.x = 0
      this.dx = this.velocity
    } else if (x + this.width > game.width) {
      this.x = game.width - this.width
      this.dx = -this.velocity
    } else if (y < 0) {
      this.y = 0
      this.dy = this.velocity
    } else if (y + this.height > game.height) {
      game.over('Game over')
    }
  },
  onTheLeftSide(platform) {
    return (this.x + this.width / 2) < (platform.x + platform.width / 2)
  },
  bumpPlatform(platform) {
    this.dy = -this.velocity
    this.dx = this.onTheLeftSide(platform) ? -this.velocity : this.velocity
  }
}

game.platform = {
  x: 300,
  y: 300,
  velocity: 6,
  dx: 0,
  ball: game.ball,
  width: 104,
  height: 24,
  move() {
    this.x += this.dx
    if (this.ball) {
      this.ball.x += this.dx
    }
  },
  stop() {
    if (this.ball) {
      this.ball.dx = 0
    }

    this.dx = 0
  },
  releaseBall() {
    if (this.ball) {
      this.ball.jump()
      this.ball = false
    }
  },

}

window.addEventListener('load', function () {
  game.start()
})
